package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

public class BowAdapter implements Weapon {

    private Bow bow;
    private boolean aimMode;

    public BowAdapter(Bow bow) {
        this.bow = bow;
        this.aimMode = false;
    }

    @Override
    public String normalAttack() {
        return this.bow.shootArrow(this.aimMode);
    }

    @Override
    public String chargedAttack() {
        this.aimMode = !this.aimMode;
        if (this.aimMode) {
            return "Entered aim shot mode";
        }
        return "Entered spontaneous mode";
    }

    @Override
    public String getName() {
        return bow.getName();
    }

    @Override
    public String getHolderName() {
        return bow.getHolderName();
    }

    public boolean getAimMode() {
        return this.aimMode;
    }
}
