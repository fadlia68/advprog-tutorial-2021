package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WeaponServiceImpl implements WeaponService {

    @Autowired
    private LogRepository logRepository;
    @Autowired
    private BowRepository bowRepository;
    @Autowired
    private SpellbookRepository spellBookRepository;
    @Autowired
    private WeaponRepository weaponRepository;

    private boolean init;

    public WeaponServiceImpl() {
        logRepository = new LogRepositoryImpl();
        init = false;
    }

    @Override
    public List<Weapon> findAll() {
        if (init) return weaponRepository.findAll();

        for (Spellbook spellbook: spellBookRepository.findAll()) {
            weaponRepository.save(new SpellbookAdapter(spellbook));
        }
        for (Bow bow: bowRepository.findAll()) {
            weaponRepository.save((new BowAdapter(bow)));
        }
        init = true;
        return weaponRepository.findAll();
    }

    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        Weapon weapon = weaponRepository.findByAlias(weaponName);
        logRepository.addLog(weapon.getHolderName() + " attacked with " + weapon.getName() + (attackType == 1 ? " (charged attack) " + weapon.chargedAttack() : " (normal attack) " + weapon.normalAttack()));
        weaponRepository.save(weapon);
    }

    @Override
    public List<String> getAllLogs() {
        return logRepository.findAll();
    }
}
