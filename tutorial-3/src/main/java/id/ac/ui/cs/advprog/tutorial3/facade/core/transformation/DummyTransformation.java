package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

/**
 * Kelas ini mengimplementasikan caesar chipper
 */

public class DummyTransformation {

    private int key;

    public DummyTransformation(int key){
        this.key = key;
    }

    public DummyTransformation(){
        this(13);
    }

    public Spell encode(Spell spell){
        return process(spell, true);
    }

    public Spell decode(Spell spell){
        return process(spell, false);
    }

    private Spell process(Spell spell, boolean encode){
        String text = spell.getText();
        Codex codex = spell.getCodex();
        int shift = encode ? key : 26 - key;
        int n = text.length();
        char[] res = new char[n];
        for(int i = 0; i < n; i++){
            if (!Character.isAlphabetic(text.charAt(i))) {
                res[i] = text.charAt(i);
            } else if (Character.isUpperCase(text.charAt(i)) ) {
                int index = ((int)text.charAt(i) + shift - 65) % 26 + 65;
                res[i] = (char)index;
            } else if (Character.isLowerCase(text.charAt(i))) {
                int index = ((int)text.charAt(i) + shift - 97) % 26 + 97;
                res[i] = (char)index;
            }
        }

        return new Spell(new String(res), codex);
    }
}
