package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class DummyTransformationTest {
    private Class<?> dummyClass;

    @BeforeEach
    public void setup() throws Exception {
        dummyClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.DummyTransformation");
    }

    @Test
    public void testDummyHasEncodeMethod() throws Exception {
        Method translate = dummyClass.getDeclaredMethod("encode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testDummyEncodesCorrectly() throws Exception {
        String text = "Safira and I went to a blacksmith to forge our sword";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Fnsven naq V jrag gb n oynpxfzvgu gb sbetr bhe fjbeq";

        Spell result = new DummyTransformation().encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testDummyEncodesCorrectlyWithCustomKey() throws Exception {
        String text = "Safira and I went to a blacksmith to forge our sword";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Xfknwf fsi N bjsy yt f gqfhpxrnym yt ktwlj tzw xbtwi";

        Spell result = new DummyTransformation(5).encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testDummyHasDecodeMethod() throws Exception {
        Method translate = dummyClass.getDeclaredMethod("decode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testDummyDecodesCorrectly() throws Exception {
        String text = "Fnsven naq V jrag gb n oynpxfzvgu gb sbetr bhe fjbeq";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Safira and I went to a blacksmith to forge our sword";

        Spell result = new DummyTransformation().decode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testDummyDecodesCorrectlyWithCustomKey() throws Exception {
        String text = "Xfknwf fsi N bjsy yt f gqfhpxrnym yt ktwlj tzw xbtwi";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Safira and I went to a blacksmith to forge our sword";

        Spell result = new DummyTransformation(5).decode(spell);
        assertEquals(expected, result.getText());
    }
}
