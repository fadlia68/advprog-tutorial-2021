package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class MondoUdonIngridientsFactoryTest {

    private Class<?> mondoUdonIngridientsFactoryClass;
    private MondoUdonIngridientsFactory mondoUdonIngridientsFactory;

    @BeforeEach
    public void setup() throws Exception {
        mondoUdonIngridientsFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.MondoUdonIngridientsFactory");
        mondoUdonIngridientsFactory = new MondoUdonIngridientsFactory();
    }

    @Test
    public void testMondoUdonIngridientsFactoryClassIsConcreteClass() {
        int classModifiers = mondoUdonIngridientsFactoryClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertFalse(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testMondoUdonIngredientFactoryIsAIngridientsFactory() {
        Collection<Type> interfaces = Arrays.asList(mondoUdonIngridientsFactoryClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.IngridientsFactory")));
    }

    @Test
    public void testMondoUdonIngredientFactoryOverrideGetNoodleMethod() throws Exception {
        Method getNoodle = mondoUdonIngridientsFactoryClass.getDeclaredMethod("getNoodle");
        int methodModifiers = getNoodle.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle",
                getNoodle.getGenericReturnType().getTypeName());
        assertEquals(0, getNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testMondoUdonIngredientFactoryOverrideGetMeatMethod() throws Exception {
        Method getMeat = mondoUdonIngridientsFactoryClass.getDeclaredMethod("getMeat");
        int methodModifiers = getMeat.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat",
                getMeat.getGenericReturnType().getTypeName());
        assertEquals(0, getMeat.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testMondoUdonIngredientFactoryOverrideGetToppingMethod() throws Exception {
        Method getTopping = mondoUdonIngridientsFactoryClass.getDeclaredMethod("getTopping");
        int methodModifiers = getTopping.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping",
                getTopping.getGenericReturnType().getTypeName());
        assertEquals(0, getTopping.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testMondoUdonIngredientFactoryOverrideGetFlavorMethod() throws Exception {
        Method getFlavor = mondoUdonIngridientsFactoryClass.getDeclaredMethod("getFlavor");
        int methodModifiers = getFlavor.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor",
                getFlavor.getGenericReturnType().getTypeName());
        assertEquals(0, getFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testMondoUdonIngridientsFactoryGetNoodleReturnUdon() throws Exception {
        assertTrue(mondoUdonIngridientsFactory.getNoodle() instanceof Udon);
    }

    @Test
    public void testMondoUdonIngridientsFactoryGetMeatReturnChicken() throws Exception {
        assertTrue(mondoUdonIngridientsFactory.getMeat() instanceof Chicken);
    }

    @Test
    public void testMondoUdonIngridientsFactoryGetToppingReturnCheese() throws Exception {
        assertTrue(mondoUdonIngridientsFactory.getTopping() instanceof Cheese);
    }

    @Test
    public void testMondoUdonIngridientsFactoryGetFlavorReturnSalty() throws Exception {
        assertTrue(mondoUdonIngridientsFactory.getFlavor() instanceof Salty);
    }

}