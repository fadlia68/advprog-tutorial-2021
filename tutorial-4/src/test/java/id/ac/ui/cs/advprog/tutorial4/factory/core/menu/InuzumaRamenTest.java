package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

public class InuzumaRamenTest {
    private Class<?> inuzumaRamenClass;

    private InuzumaRamen inuzumaRamen;

    @BeforeEach
    public void setUp() throws Exception {
        inuzumaRamenClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen");
        inuzumaRamen = new InuzumaRamen("testInuzumaRamen");
    }

    @Test
    public void testInuzumaRamenIsAPublicClass() {
        int classModifiers = inuzumaRamenClass.getModifiers();
        assertTrue(Modifier.isPublic(classModifiers));
    }

    @Test
    public void testInuzumaRamenIsAConcreteClass() {
        assertFalse(Modifier.isAbstract(inuzumaRamenClass.getModifiers()));
    }

    @Test
    public void testInuzumaRamenGetNameShouldReturnName() {
        assertEquals("testInuzumaRamen", inuzumaRamen.getName());
    }

    @Test
    public void testInuzumaRamenGetNoodleShouldReturnRamen() {
        assertTrue(inuzumaRamen.getNoodle() instanceof Ramen);
    }

    @Test
    public void testInuzumaRamenGetMeatShouldReturnPork() {
        assertTrue(inuzumaRamen.getMeat() instanceof Pork);
    }

    @Test
    public void testInuzumaRamenGetToppingShouldReturnBoiledEgg() {
        assertTrue(inuzumaRamen.getTopping() instanceof BoiledEgg);
    }

    @Test
    public void testInuzumaRamenGetFlavorShouldReturnSpicy() {
        assertTrue(inuzumaRamen.getFlavor() instanceof Spicy);
    }
}
