package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

public class MondoUdonTest {
    private Class<?> mondoUdonClass;

    private MondoUdon mondoUdon;

    @BeforeEach
    public void setUp() throws Exception {
        mondoUdonClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.MondoUdon");
        mondoUdon = new MondoUdon("testMondoUdon");
    }

    @Test
    public void testMondoUdonIsAPublicClass() {
        int classModifiers = mondoUdonClass.getModifiers();
        assertTrue(Modifier.isPublic(classModifiers));
    }

    @Test
    public void testMondoUdonIsAConcreteClass() {
        assertFalse(Modifier.isAbstract(mondoUdonClass.getModifiers()));
    }

    @Test
    public void testMondoUdonGetNameShouldReturnName() {
        assertEquals("testMondoUdon", mondoUdon.getName());
    }

    @Test
    public void testMondoUdonGetNoodleShouldReturnUdon() {
        assertTrue(mondoUdon.getNoodle() instanceof Udon);
    }

    @Test
    public void testMondoUdonGetMeatShouldReturnChicken() {
        assertTrue(mondoUdon.getMeat() instanceof Chicken);
    }

    @Test
    public void testMondoUdonGetToppingShouldReturnCheese() {
        assertTrue(mondoUdon.getTopping() instanceof Cheese);
    }

    @Test
    public void testMondoUdonGetFlavorShouldReturnSalty() {
        assertTrue(mondoUdon.getFlavor() instanceof Salty);
    }
}
