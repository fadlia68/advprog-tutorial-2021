package id.ac.ui.cs.advprog.tutorial4.singleton.core;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class OrderFoodTest {

    @Test
    public void testOrderFoodGetSameInstance() throws Exception {
        OrderFood orderFoodA = OrderFood.getInstance();
        OrderFood orderFoodB = OrderFood.getInstance();

        assertEquals(orderFoodA, orderFoodB);
    }

    @Test
    public void testOrderFoodGetAndSetDrinkImplementedCorrectly() throws Exception {
        OrderFood orderFood = OrderFood.getInstance();
        orderFood.setFood("testOrderFood");
        assertEquals(orderFood.getFood(), "testOrderFood");
    }
}
