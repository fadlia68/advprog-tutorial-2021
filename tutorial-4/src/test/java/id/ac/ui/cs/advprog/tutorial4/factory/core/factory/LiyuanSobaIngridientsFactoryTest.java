package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class LiyuanSobaIngridientsFactoryTest {

    private Class<?> liyuanSobaIngridientsFactoryClass;
    private LiyuanSobaIngridientsFactory liyuanSobaIngridientsFactory;

    @BeforeEach
    public void setup() throws Exception {
        liyuanSobaIngridientsFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.LiyuanSobaIngridientsFactory");
        liyuanSobaIngridientsFactory = new LiyuanSobaIngridientsFactory();
    }

    @Test
    public void testLiyuanSobaIngridientsFactoryClassIsConcreteClass() {
        int classModifiers = liyuanSobaIngridientsFactoryClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertFalse(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testLiyuanSobaIngredientFactoryIsAIngridientsFactory() {
        Collection<Type> interfaces = Arrays.asList(liyuanSobaIngridientsFactoryClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.IngridientsFactory")));
    }

    @Test
    public void testLiyuanSobaIngredientFactoryOverrideGetNoodleMethod() throws Exception {
        Method getNoodle = liyuanSobaIngridientsFactoryClass.getDeclaredMethod("getNoodle");
        int methodModifiers = getNoodle.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle",
                getNoodle.getGenericReturnType().getTypeName());
        assertEquals(0, getNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testLiyuanSobaIngredientFactoryOverrideGetMeatMethod() throws Exception {
        Method getMeat = liyuanSobaIngridientsFactoryClass.getDeclaredMethod("getMeat");
        int methodModifiers = getMeat.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat",
                getMeat.getGenericReturnType().getTypeName());
        assertEquals(0, getMeat.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testLiyuanSobaIngredientFactoryOverrideGetToppingMethod() throws Exception {
        Method getTopping = liyuanSobaIngridientsFactoryClass.getDeclaredMethod("getTopping");
        int methodModifiers = getTopping.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping",
                getTopping.getGenericReturnType().getTypeName());
        assertEquals(0, getTopping.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testLiyuanSobaIngredientFactoryOverrideGetFlavorMethod() throws Exception {
        Method getFlavor = liyuanSobaIngridientsFactoryClass.getDeclaredMethod("getFlavor");
        int methodModifiers = getFlavor.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor",
                getFlavor.getGenericReturnType().getTypeName());
        assertEquals(0, getFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testLiyuanSobaIngridientsFactoryGetNoodleReturnSoba() throws Exception {
        assertTrue(liyuanSobaIngridientsFactory.getNoodle() instanceof Soba);
    }

    @Test
    public void testLiyuanSobaIngridientsFactoryGetMeatReturnBeef() throws Exception {
        assertTrue(liyuanSobaIngridientsFactory.getMeat() instanceof Beef);
    }

    @Test
    public void testLiyuanSobaIngridientsFactoryGetToppingReturnMushroom() throws Exception {
        assertTrue(liyuanSobaIngridientsFactory.getTopping() instanceof Mushroom);
    }

    @Test
    public void testLiyuanSobaIngridientsFactoryGetFlavorReturnSweet() throws Exception {
        assertTrue(liyuanSobaIngridientsFactory.getFlavor() instanceof Sweet);
    }

}