package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class IngridientsFactoryTest {
    private Class<?> ingridientsFactoryClass;

    @BeforeEach
    public void setup() throws Exception {
        ingridientsFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.IngridientsFactory");
    }

    @Test
    public void testIngridientsFactoryIsAPublicInterface() {
        int classModifiers = ingridientsFactoryClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testIngridientsFactoryHasGetNoodleAbstractMethod() throws Exception {
        Method getNoodle = ingridientsFactoryClass.getDeclaredMethod("getNoodle");
        int methodModifiers = getNoodle.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getNoodle.getParameterCount());
    }

    @Test
    public void testIngridientsFactoryHasGetMeatAbstractMethod() throws Exception {
        Method getMeat = ingridientsFactoryClass.getDeclaredMethod("getMeat");
        int methodModifiers = getMeat.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getMeat.getParameterCount());
    }

    @Test
    public void testIngridientsFactoryHasGetToppingAbstractMethod() throws Exception {
        Method getTopping = ingridientsFactoryClass.getDeclaredMethod("getTopping");
        int methodModifiers = getTopping.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getTopping.getParameterCount());
    }

    @Test
    public void testIngridientsFactoryHasGetFlavorAbstractMethod() throws Exception {
        Method getFlavor = ingridientsFactoryClass.getDeclaredMethod("getFlavor");
        int methodModifiers = getFlavor.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getFlavor.getParameterCount());
    }

}