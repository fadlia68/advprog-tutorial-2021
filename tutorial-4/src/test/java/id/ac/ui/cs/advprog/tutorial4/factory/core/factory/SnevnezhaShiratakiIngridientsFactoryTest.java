package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class SnevnezhaShiratakiIngridientsFactoryTest {

    private Class<?> snevnezhaShiratakiIngridientsFactoryClass;
    private SnevnezhaShiratakiIngridientsFactory snevnezhaShiratakiIngridientsFactory;

    @BeforeEach
    public void setup() throws Exception {
        snevnezhaShiratakiIngridientsFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.SnevnezhaShiratakiIngridientsFactory");
        snevnezhaShiratakiIngridientsFactory = new SnevnezhaShiratakiIngridientsFactory();
    }

    @Test
    public void testSnevnezhaShiratakiIngridientsFactoryClassIsConcreteClass() {
        int classModifiers = snevnezhaShiratakiIngridientsFactoryClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertFalse(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testSnevnezhaShiratakiIngredientFactoryIsAIngridientsFactory() {
        Collection<Type> interfaces = Arrays.asList(snevnezhaShiratakiIngridientsFactoryClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.IngridientsFactory")));
    }

    @Test
    public void testSnevnezhaShiratakiIngredientFactoryOverrideGetNoodleMethod() throws Exception {
        Method getNoodle = snevnezhaShiratakiIngridientsFactoryClass.getDeclaredMethod("getNoodle");
        int methodModifiers = getNoodle.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle",
                getNoodle.getGenericReturnType().getTypeName());
        assertEquals(0, getNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testSnevnezhaShiratakiIngredientFactoryOverrideGetMeatMethod() throws Exception {
        Method getMeat = snevnezhaShiratakiIngridientsFactoryClass.getDeclaredMethod("getMeat");
        int methodModifiers = getMeat.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat",
                getMeat.getGenericReturnType().getTypeName());
        assertEquals(0, getMeat.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testSnevnezhaShiratakiIngredientFactoryOverrideGetToppingMethod() throws Exception {
        Method getTopping = snevnezhaShiratakiIngridientsFactoryClass.getDeclaredMethod("getTopping");
        int methodModifiers = getTopping.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping",
                getTopping.getGenericReturnType().getTypeName());
        assertEquals(0, getTopping.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testSnevnezhaShiratakiIngredientFactoryOverrideGetFlavorMethod() throws Exception {
        Method getFlavor = snevnezhaShiratakiIngridientsFactoryClass.getDeclaredMethod("getFlavor");
        int methodModifiers = getFlavor.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor",
                getFlavor.getGenericReturnType().getTypeName());
        assertEquals(0, getFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testSnevnezhaShiratakiIngridientsFactoryGetNoodleReturnShirataki() throws Exception {
        assertTrue(snevnezhaShiratakiIngridientsFactory.getNoodle() instanceof Shirataki);
    }

    @Test
    public void testSnevnezhaShiratakiIngridientsFactoryGetMeatReturnFish() throws Exception {
        assertTrue(snevnezhaShiratakiIngridientsFactory.getMeat() instanceof Fish);
    }

    @Test
    public void testSnevnezhaShiratakiIngridientsFactoryGetToppingReturnFlower() throws Exception {
        assertTrue(snevnezhaShiratakiIngridientsFactory.getTopping() instanceof Flower);
    }

    @Test
    public void testSnevnezhaShiratakiIngridientsFactoryGetFlavorReturnUmami() throws Exception {
        assertTrue(snevnezhaShiratakiIngridientsFactory.getFlavor() instanceof Umami);
    }

}