package id.ac.ui.cs.advprog.tutorial4.singleton.core;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class OrderDrinkTest {

    @Test
    public void testOrderDrinkGetSameInstance() throws Exception {
        OrderDrink orderDrinkA = OrderDrink.getInstance();
        OrderDrink orderDrinkB = OrderDrink.getInstance();

        assertEquals(orderDrinkA, orderDrinkB);
    }

    @Test
    public void testOrderDrinkGetAndSetDrinkImplementedCorrectly() throws Exception {
        OrderDrink orderDrink = OrderDrink.getInstance();
        orderDrink.setDrink("testOrderDrink");
        assertEquals(orderDrink.getDrink(), "testOrderDrink");
    }
}
