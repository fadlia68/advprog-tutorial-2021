package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class FlowerTest {
    private Class<?> flowerClass;

    @BeforeEach
    public void setup() throws Exception {
        flowerClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower");
    }

    @Test
    public void testFlowerIsConcreteClass() {
        int classModifiers = flowerClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertFalse(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testFlowerIsATopping() {
        Collection<Type> interfaces = Arrays.asList(flowerClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping")));
    }

    @Test
    public void testFlowerOverrideGetDescriptionMethod() throws Exception {
        Method getDescription = flowerClass.getDeclaredMethod("getDescription");
        int methodModifiers = getDescription.getModifiers();

        assertEquals("java.lang.String", getDescription.getGenericReturnType().getTypeName());
        assertEquals(0, getDescription.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testFlowerGetDescriptionShouldReturnDescription() throws Exception {
        assertEquals("Adding Xinqin Flower Topping...", new Flower().getDescription());
    }
}
