package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class OrderServiceTest {
    private Class<?> orderServiceClass;

    @BeforeEach
    public void setup() throws Exception {
        orderServiceClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.singleton.service.OrderService");
    }

    @Test
    public void testOrderServiceIsAPublicInterface() {
        int classModifiers = orderServiceClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testOrderServiceHasGetDrinkAbstractMethod() throws Exception {
        Method getDrink = orderServiceClass.getDeclaredMethod("getDrink");
        int methodModifiers = getDrink.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getDrink.getParameterCount());
    }

    @Test
    public void testOrderServiceHasGetFoodAbstractMethod() throws Exception {
        Method getFood = orderServiceClass.getDeclaredMethod("getFood");
        int methodModifiers = getFood.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getFood.getParameterCount());
    }

    @Test
    public void testOrderServiceHasVoidOrderDrinkAbstractMethod() throws Exception {

        Method orderADrink = orderServiceClass.getDeclaredMethod("orderADrink", String.class);
        int methodModifiers = orderADrink.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(1, orderADrink.getParameterCount());
    }

    @Test
    public void testOrderServiceHasVoidOrderFoodAbstractMethod() throws Exception {

        Method orderAFood = orderServiceClass.getDeclaredMethod("orderAFood", String.class);
        int methodModifiers = orderAFood.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(1, orderAFood.getParameterCount());
    }

}