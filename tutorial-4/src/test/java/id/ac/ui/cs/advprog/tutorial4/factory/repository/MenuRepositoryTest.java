package id.ac.ui.cs.advprog.tutorial4.factory.repository;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

public class MenuRepositoryTest {

    @Mock
    private Menu menu;

    @Test
    public void testAddMenuShouldImplementedCorrectly() {
        
        MenuRepository menuRepository = new MenuRepository();
        menu = mock(Menu.class);

        assertTrue(menuRepository.add(menu) instanceof Menu);
        assertEquals(menuRepository.getMenus().size(), 1);
    }

}
