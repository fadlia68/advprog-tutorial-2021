package id.ac.ui.cs.advprog.tutorial4.factory.service;


import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import id.ac.ui.cs.advprog.tutorial4.factory.repository.MenuRepository;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MenuServiceImplTest {

    private MenuServiceImpl menuService;

    @Mock
    private MenuRepository menuRepository;
    @Mock
    private Menu menu;

    @Test
    public void testMenuServiceImplHasGetMenusImplementation() {
        menuService = new MenuServiceImpl();
        assertEquals(menuService.getMenus().size(), 4);
    }

    @Test
    public void testMenuServiceImplHasCreateMenuImplementation() {

        menuService = new MenuServiceImpl();
        menuRepository = mock(MenuRepository.class);
        menu = mock(Menu.class);

        when(menuRepository.add(menu)).thenReturn(menu);
        assertTrue(menuService.createMenu("testSoba", "Soba") instanceof Menu);
        assertTrue(menuService.createMenu("testUdon", "Udon") instanceof Menu);
        assertTrue(menuService.createMenu("testShirataki", "Shirataki") instanceof Menu);
        assertTrue(menuService.createMenu("testRamen", "Ramen") instanceof Menu);

        assertEquals(menuService.getMenus().size(), 8);
    }
}
