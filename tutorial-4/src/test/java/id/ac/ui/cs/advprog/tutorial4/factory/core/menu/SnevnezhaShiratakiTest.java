package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

public class SnevnezhaShiratakiTest {
    private Class<?> snevnezhaShiratakiClass;

    private SnevnezhaShirataki snevnezhaShirataki;

    @BeforeEach
    public void setUp() throws Exception {
        snevnezhaShiratakiClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.SnevnezhaShirataki");
        snevnezhaShirataki = new SnevnezhaShirataki("testSnevnezhaShirataki");
    }

    @Test
    public void testSnevnezhaShiratakiIsAPublicClass() {
        int classModifiers = snevnezhaShiratakiClass.getModifiers();
        assertTrue(Modifier.isPublic(classModifiers));
    }

    @Test
    public void testSnevnezhaShiratakiIsAConcreteClass() {
        assertFalse(Modifier.isAbstract(snevnezhaShiratakiClass.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiGetNameShouldReturnName() {
        assertEquals("testSnevnezhaShirataki", snevnezhaShirataki.getName());
    }

    @Test
    public void testSnevnezhaShiratakiGetNoodleShouldReturnShirataki() {
        assertTrue(snevnezhaShirataki.getNoodle() instanceof Shirataki);
    }

    @Test
    public void testSnevnezhaShiratakiGetMeatShouldReturnFish() {
        assertTrue(snevnezhaShirataki.getMeat() instanceof Fish);
    }

    @Test
    public void testSnevnezhaShiratakiGetToppingShouldReturnFlower() {
        assertTrue(snevnezhaShirataki.getTopping() instanceof Flower);
    }

    @Test
    public void testSnevnezhaShiratakiGetFlavorShouldReturnUmami() {
        assertTrue(snevnezhaShirataki.getFlavor() instanceof Umami);
    }
}
