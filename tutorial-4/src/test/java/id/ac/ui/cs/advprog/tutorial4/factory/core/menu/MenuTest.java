package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.IngridientsFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MenuTest {
    private Class<?> menuClass;
    private Menu menu;

    @Mock
    private IngridientsFactory ingridientsFactory;
    @Mock
    private Noodle noodle;
    @Mock
    private Flavor flavor;
    @Mock
    private Meat meat;
    @Mock
    private Topping topping;

    @BeforeEach
    public void setUp() throws Exception {
        menuClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu");
        ingridientsFactory = mock(IngridientsFactory.class);
        noodle = mock(Noodle.class);
        topping = mock(Topping.class);
        flavor = mock(Flavor.class);
        meat = mock(Meat.class);
        menu = new Menu("testMenu", ingridientsFactory);
    }

    @Test
    public void testMenuIsAPublicClass() {
        int classModifiers = menuClass.getModifiers();
        assertTrue(Modifier.isPublic(classModifiers));
    }

    @Test
    public void testMenuHasGetNameMethod() throws Exception {
        Method getName = menuClass.getDeclaredMethod("getName");
        int methodModifiers = getName.getModifiers();

        assertEquals("java.lang.String", getName.getGenericReturnType().getTypeName());
        assertEquals(0, getName.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testMenuHasGetNoodleMethod() throws Exception {
        Method getNoodle = menuClass.getDeclaredMethod("getNoodle");
        int methodModifiers = getNoodle.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle",
                getNoodle.getGenericReturnType().getTypeName());
        assertEquals(0, getNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testMenuHasGetMeatMethod() throws Exception {
        Method getMeat = menuClass.getDeclaredMethod("getMeat");
        int methodModifiers = getMeat.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat",
                getMeat.getGenericReturnType().getTypeName());
        assertEquals(0, getMeat.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testMenuHasGetToppingMethod() throws Exception {
        Method getTopping = menuClass.getDeclaredMethod("getTopping");
        int methodModifiers = getTopping.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping", getTopping.getGenericReturnType().getTypeName());
        assertEquals(0, getTopping.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testMenuHasGetFlavorMethod() throws Exception {
        Method getFlavor = menuClass.getDeclaredMethod("getFlavor");
        int methodModifiers = getFlavor.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor", getFlavor.getGenericReturnType().getTypeName());
        assertEquals(0, getFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testMenuHasCreateMethod() throws Exception {
        Method create = menuClass.getDeclaredMethod("create");
        int methodModifiers = create.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(0, create.getParameterCount());
    }

    @Test
    public void testMenuCreateMethodImplementedCorrectly() throws Exception {

        when(ingridientsFactory.getNoodle()).thenReturn(noodle);
        when(ingridientsFactory.getMeat()).thenReturn(meat);
        when(ingridientsFactory.getTopping()).thenReturn(topping);
        when(ingridientsFactory.getFlavor()).thenReturn(flavor);

        menu.create();

        assertThat(menu.getNoodle()).isInstanceOf(Noodle.class);
        assertThat(menu.getFlavor()).isInstanceOf(Flavor.class);
        assertThat(menu.getMeat()).isInstanceOf(Meat.class);
        assertThat(menu.getTopping()).isInstanceOf(Topping.class);
    }

    @Test
    public void testMenuGetNameMethodImplementedCorrectly() {
        assertEquals("testMenu", menu.getName());
    }
}
