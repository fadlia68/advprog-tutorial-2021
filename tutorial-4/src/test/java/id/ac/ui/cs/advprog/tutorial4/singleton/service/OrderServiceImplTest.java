package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink;
import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderFood;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class OrderServiceImplTest {

    private OrderServiceImpl orderService;

    @BeforeEach
    public void setup() throws Exception {
        orderService = new OrderServiceImpl();
    }

    @Test
    public void testOrderServiceImplHasOrderAndGetDrinkImplementation() {
        orderService.orderADrink("testDrink");
        assertTrue(orderService.getDrink() instanceof OrderDrink);
        assertEquals(orderService.getDrink().getDrink(), "testDrink");
    }

    @Test
    public void testOrderServiceImplHasOrderAndGetFoodImplementation() {
        orderService.orderAFood("testFood");
        assertTrue(orderService.getFood() instanceof OrderFood);
        assertEquals(orderService.getFood().getFood(), "testFood");
    }

}
