package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class InuzumaRamenIngridientsFactoryTest {

    private Class<?> inuzumaRamenIngridientsFactoryClass;
    private InuzumaRamenIngridientsFactory inuzumaRamenIngridientsFactory;

    @BeforeEach
    public void setup() throws Exception {
        inuzumaRamenIngridientsFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.InuzumaRamenIngridientsFactory");
        inuzumaRamenIngridientsFactory = new InuzumaRamenIngridientsFactory();
    }

    @Test
    public void testinuzumaRamenIngridientsFactoryClassIsConcreteClass() {
        int classModifiers = inuzumaRamenIngridientsFactoryClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertFalse(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testInuzumaRamenIngredientFactoryIsAIngridientsFactory() {
        Collection<Type> interfaces = Arrays.asList(inuzumaRamenIngridientsFactoryClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.IngridientsFactory")));
    }

    @Test
    public void testInuzumaRamenIngredientFactoryOverrideGetNoodleMethod() throws Exception {
        Method getNoodle = inuzumaRamenIngridientsFactoryClass.getDeclaredMethod("getNoodle");
        int methodModifiers = getNoodle.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle",
                getNoodle.getGenericReturnType().getTypeName());
        assertEquals(0, getNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testInuzumaRamenIngredientFactoryOverrideGetMeatMethod() throws Exception {
        Method getMeat = inuzumaRamenIngridientsFactoryClass.getDeclaredMethod("getMeat");
        int methodModifiers = getMeat.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat",
                getMeat.getGenericReturnType().getTypeName());
        assertEquals(0, getMeat.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testInuzumaRamenIngredientFactoryOverrideGetToppingMethod() throws Exception {
        Method getTopping = inuzumaRamenIngridientsFactoryClass.getDeclaredMethod("getTopping");
        int methodModifiers = getTopping.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping",
                getTopping.getGenericReturnType().getTypeName());
        assertEquals(0, getTopping.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testInuzumaRamenIngredientFactoryOverrideGetFlavorMethod() throws Exception {
        Method getFlavor = inuzumaRamenIngridientsFactoryClass.getDeclaredMethod("getFlavor");
        int methodModifiers = getFlavor.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor",
                getFlavor.getGenericReturnType().getTypeName());
        assertEquals(0, getFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testInuzumaRamenIngridientsFactoryGetNoodleReturnRamen() throws Exception {
        assertTrue(inuzumaRamenIngridientsFactory.getNoodle() instanceof Ramen);
    }

    @Test
    public void testInuzumaRamenIngridientsFactoryGetMeatReturnPork() throws Exception {
        assertTrue(inuzumaRamenIngridientsFactory.getMeat() instanceof Pork);
    }

    @Test
    public void testInuzumaRamenIngridientsFactoryGetToppingReturnBoiledEgg() throws Exception {
        assertTrue(inuzumaRamenIngridientsFactory.getTopping() instanceof BoiledEgg);
    }

    @Test
    public void testInuzumaRamenIngridientsFactoryGetFlavorReturnSpicy() throws Exception {
        assertTrue(inuzumaRamenIngridientsFactory.getFlavor() instanceof Spicy);
    }

}