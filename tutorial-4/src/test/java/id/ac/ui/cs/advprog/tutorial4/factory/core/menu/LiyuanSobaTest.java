package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

public class LiyuanSobaTest {
    private Class<?> liyuanSobaClass;

    private LiyuanSoba liyuanSoba;

    @BeforeEach
    public void setUp() throws Exception {
        liyuanSobaClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba");
        liyuanSoba = new LiyuanSoba("testLiyuanSoba");
    }

    @Test
    public void testLiyuanSobaIsAPublicClass() {
        int classModifiers = liyuanSobaClass.getModifiers();
        assertTrue(Modifier.isPublic(classModifiers));
    }

    @Test
    public void testLiyuanSobaIsAConcreteClass() {
        assertFalse(Modifier.isAbstract(liyuanSobaClass.getModifiers()));
    }

    @Test
    public void testLiyuanSobaGetNameShouldReturnName() {
        assertEquals("testLiyuanSoba", liyuanSoba.getName());
    }

    @Test
    public void testLiyuanSobaGetNoodleShouldReturnSoba() {
        assertTrue(liyuanSoba.getNoodle() instanceof Soba);
    }

    @Test
    public void testLiyuanSobaGetMeatShouldReturnBeef() {
        assertTrue(liyuanSoba.getMeat() instanceof Beef);
    }

    @Test
    public void testLiyuanSobaGetToppingShouldReturnMushroom() {
        assertTrue(liyuanSoba.getTopping() instanceof Mushroom);
    }

    @Test
    public void testLiyuanSobaGetFlavorShouldReturnSweet() {
        assertTrue(liyuanSoba.getFlavor() instanceof Sweet);
    }
}
