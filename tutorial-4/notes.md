### Lazy Instantiation and Eager Instantiation

Pada Singleton Pattern dengan approach Lazy Instantiation sebuah instansi akan dibuat hanya jika diperlukan, bukan saat class tersebut di-load. Sementara pada approach Eager Instantiation sebuah instansi akan otomatis dibuat saat class tersebut di-load, baik itu akan digunakan ataupun tidak. 

#### Lazy Instantiation
Kelebihan: Menambah performance dari aplikasi kita apabila objek singleton yang ingin dibuat cukup banyak sementara belum kita perlukan.
Kelemahan: Waktu load time akan terbagi pada waktu inisiasi di awal dan pada waktu running program di pertengahan.

#### Eager Instantiation
Kelebihan: Running time program tidak akan terganggu oleh pembuatan objek singleton karena pembuatannya sudah diselesaikan pada awal inisiasi program.
Kekurangan: Memakan banyak waktu apabila kita sedang tidak perlu memakai seluruh objek singleton yang sudah kita desain.